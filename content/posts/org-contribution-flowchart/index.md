+++
title = "Org Contribution Flow-chart"
author = ["Kaushal Modi"]
description = """
  A handy flow-chart if you are confused about when commits happen to
  Org `maint` branch vs `master` branch, what ends up in the Elpa
  version of Org, and so on.
  """
date = 2018-03-06T00:23:00-05:00
images = ["flowchart-cropped.png"]
tags = ["plantuml", "flow-chart", "contribution", "development"]
categories = ["emacs", "org"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.13 + ox-hugo)"
[syndication]
  twitter = 970896341999194112
[versions]
  plantuml = "1.2018.2"
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Flow chart](#flow-chart)
- [PlantUML Source](#org-devel-flowchart-source)
- [PlantUML Hints](#plantuml-hints)
- [Reference](#reference)

</div>
<!--endtoc-->

I have often seen questions and confusion about why certain things got
fixed in Org, or a new feature got added, but then did not show up in
the next Org update via Elpa.

The below flow chart is an attempt to answer all such questions, and
also my first attempt at creating one using **PlantUML** (_legacy
syntax_[^fn:1]).

<div class="note">
  <div></div>

When I tried the new (beta) syntax, it did not allow using, what I
call, "labels".. see the `as maint`, `as master` syntax in the
flowchart [source](#org-devel-flowchart-source).

</div>


## Flow chart {#flow-chart}

{{< figure src="flowchart.png" caption="Figure 1: Flow of a commit in Org mode repo from `maint` branch to `master` branch to Emacs repo" >}}


## PlantUML Source {#org-devel-flowchart-source}

```plantuml
(*) --> "My Org commit"

--> "Discuss on Org mailing list"

if "Bug fix commit?" then
  -->[yes] "Commit to Org **maint**" as maint
else
  ->[no] if "**maint** compatible doc fix commit?" then
    -->[yes] maint
  else
    ->[no] "Commit to Org **master**" as master
  endif
endif

maint --> "Merge to Org **master**"
  --> master
maint --> "Wait till next Monday"
  --> "Push to Org Elpa" as push
maint --> "Short maturity time"
  --> "Cut a minor release" as minor_release

minor_release --> ===RELEASE===
minor_release --> master

master --> "**Long maturity time**"
  --> "Cut a major release" as major_release

major_release --> ===RELEASE===
major_release --> "Merge to Org **maint**"
  --> maint

===RELEASE=== --> push
===RELEASE=== --> "Squash and commit to Emacs release or master branch"
  --> (*)

push --> (*)
```


## PlantUML Hints {#plantuml-hints}

| Syntax      | Output           |
|-------------|------------------|
| `-​-​>`       | Vertical arrow   |
| `-​>`        | Horizontal arrow |
| `(*) -​-​>`   | Start point      |
| `-​-​> (*)`   | End point        |


## Reference {#reference}

-   [PlantUML Language Reference Guide (pdf)](http://plantuml.com/PlantUML%5FLanguage%5FReference%5FGuide.pdf)

[^fn:1]: [Legacy](http://plantuml.com/activity-diagram-legacy) vs [new (beta)](http://plantuml.com/activity-diagram-beta) PlantUML syntax for activity diagrams

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
