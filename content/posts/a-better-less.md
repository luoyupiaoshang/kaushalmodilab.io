+++
date = "2017-04-24T17:54:51-04:00"
title = "A Better less"
categories = ["emacs", "bash"]

+++

I use emacs -- with a [server][1][^fn:1] and client setup -- so that I
can quickly open new files in it using `emacsclient`. My typical setup
is to have `xterm` + `tmux` on one monitor and a single emacs(client)
frame on another. But there are times when I do not want to
shift focus from one monitor to another, like when I just need to
review some log file. So I started using an alias to `emacs -nw -Q` to
quickly open log files, review, filter, and `C-x C-c`.

Of course, that's not where this post ends.  An alias was no longer
adequate to do what I wanted it to do more ..

<!--more-->

I typically do my text editing in the emacs frame, while do just
non-editing actions like viewing, navigating, searching, filtering in
quick emacs sessions from `tmux`.

I wanted that "tmux emacs" to be independent of my config, independent
of the server -- Just something really quick that I can launch, *do*,
and quit. I wanted something *like* `less`, but better .. better in
these ways:

- Do syntax highlighting
- Render Org-mode files
- A better navigable man page viewer
- Dired, especially `wdired` (batch edit symbolic links, for example?)[^fn:2]
- Show colored diffs
- Filter log files to only show or not show lines matching a regexp
- Start auto-reverting log files when I want (like `tail -f`)
- Quickly change frame and font sizes
- .. and more; basically everything that emacs has to offer!

I call it `eless` and here's a little taste of what it looks like:

*Click the below image to see a GIF animation in larger size.*
[![eless][5]][6]

As a bonus:

- [x] This script passes [ShellCheck][2], and
- [x] Unofficial Bash [strict mode][3] is enabled.

[Try it out][4] and let me know how you find it. As you will see,
`eless` is a bash script where most of it is a string containing a
*sane* emacs configuration for `view-mode`. I wanted `eless` to be a
monolithic script, and not a multi-file setup.

```bash
eless -h | eless
```

---

**Update (2017/04/25)**

Thanks to the [PR][7] by [Iqbal Ansari][8], `eless` now supports
reading piped-in data even when emacs is run in terminal mode. The
solution was to:

- Always start the emacs process in a sub-shell, and
- Redirect `/dev/tty` into that emacs instance.

That way, that emacs instance would always *think* that it is
receiving input from tty -- even when the wrapper bash script is
receiving input from the pipe.

[^fn:1]: You can also do <kbd>C-h</kbd><kbd>i</kbd><kbd>g</kbd> `(emacs) Emacs Server` from within emacs.
[^fn:2]: Do: <kbd>C-h</kbd><kbd>i</kbd><kbd>g</kbd> `(emacs) Wdired`

[1]: https://www.gnu.org/software/emacs/manual/html_node/emacs/Emacs-Server.html
[2]: https://www.shellcheck.net
[3]: http://redsymbol.net/articles/unofficial-bash-strict-mode
[4]: https://github.com/kaushalmodi/eless
[5]: /images/eless.png
[6]: /gifs/eless.gif
[7]: https://github.com/kaushalmodi/eless/pull/2
[8]: https://github.com/iqbalansari
